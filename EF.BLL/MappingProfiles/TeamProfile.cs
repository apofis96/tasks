﻿using AutoMapper;
using EF.Common.DTO.Team;
using EF.DAL.Models;

namespace EF.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
            CreateMap<TeamCreateDTO, Team>();
        }
    }
}
