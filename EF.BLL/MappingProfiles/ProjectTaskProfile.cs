﻿using AutoMapper;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.DAL.Models;

namespace EF.BLL.MappingProfiles
{
    public sealed class ProjectTaskProfile : Profile
    {
        public ProjectTaskProfile()
        {
            CreateMap<ProjectTask, ProjectTaskDTO>();
            CreateMap<ProjectTaskDTO, ProjectTask>();
            CreateMap<ProjectTaskDTO, FinishedTaskDTO>();
            CreateMap<ProjectTaskCreateDTO, ProjectTask>();
        }
    }
}
