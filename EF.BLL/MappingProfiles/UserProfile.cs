﻿using AutoMapper;
using EF.Common.DTO.User;
using EF.DAL.Models;

namespace EF.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
            CreateMap<UserCreateDTO, User>();
        }
    }
}
