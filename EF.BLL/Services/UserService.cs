﻿using AutoMapper;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ProjectTask> _taskRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public UserService(IRepository<User> userRepository, IRepository<ProjectTask> taskRepository, IMapper mapper, IExistService existService)
        {
            _userRepository = userRepository;
            _taskRepository = taskRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _userRepository.GetAsync();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
        public async Task<UserDTO> GetAsync(int id)
        {
            await _existService.UserIsExistOrExeptionAsync(id);
            var user = (await _userRepository.GetAsync(IRepository<User>.FilterById(id))).FirstOrDefault();
            return _mapper.Map<UserDTO>(user);
        }
        public async Task<UserDTO> PostAsync(UserCreateDTO user)
        {
            if (user.TeamId.HasValue)
                await _existService.TeamIsExistOrExeptionAsync(user.TeamId.Value);
            var newUser = _mapper.Map<User>(user);
            await _userRepository.CreateAsync(newUser);
            return await GetAsync(newUser.Id);
        }
        public async Task<UserDTO> UpdateAsync(UserUpdateDTO updateUser)
        {
            if (updateUser.TeamId.HasValue)
                await _existService.TeamIsExistOrExeptionAsync(updateUser.TeamId.Value);
            await _existService.UserIsExistOrExeptionAsync(updateUser.Id.Value);
            var user = (await _userRepository.GetAsync(IRepository<User>.FilterById(updateUser.Id.Value))).FirstOrDefault();
            user.FirstName = updateUser.FirstName;
            user.LastName = updateUser.LastName;
            user.Email = updateUser.Email;
            user.Birthday = updateUser.Birthday.Value;
            user.TeamId = updateUser.TeamId;
            await _userRepository.UpdateAsync(user);
            return await GetAsync(updateUser.Id.Value);
        }
        public async Task DeleteAsync(int id)
        {
            await _existService.UserIsExistOrExeptionAsync(id);
            await _userRepository.DeleteAsync(id);
        }
        public async Task<IEnumerable<UserTasksDTO>> GetUserTasksAsync()
        {
            var taskUsers = _userRepository.GetAsync();
            var taskTasks = _taskRepository.GetAsync();
            await Task.WhenAll(taskTasks, taskUsers);
            return (await taskUsers).OrderBy(u => u.FirstName)
                .GroupJoin((await taskTasks).OrderByDescending(t => t.Name.Length).Join((await taskUsers),
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u;
                    return t;
                }),
                u => u.Id, t => t.PerformerId, (u, t) => new UserTasksDTO
                {
                    User = _mapper.Map<UserDTO>(u),
                    Tasks = _mapper.Map<IEnumerable<ProjectTaskDTO>>(t)
                });
        }
    }
}