﻿using AutoMapper;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using EF.Common;
using System.Threading.Tasks;

namespace EF.BLL.Services
{
    public class ProjectTaskService : IProjectTaskService
    {
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public ProjectTaskService(IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository, IMapper mapper, IExistService existService)
        {
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<ProjectTaskDTO>> GetAllAsync()
        {
            var taskTasks = _projectTaskRepository.GetAsync();
            var taskUsers = _userRepository.GetAsync();
            await Task.WhenAll(taskTasks, taskUsers);
            var tasks = (await taskTasks).GroupJoin((await taskUsers),
                t => t.PerformerId, u => u.Id, (t, u) =>
                 {
                     t.Performer = u.FirstOrDefault();
                     return t;
                 });
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>(tasks);
        }
        public async Task<ProjectTaskDTO> GetAsync(int id)
        {
            await _existService.ProjectTaskIsExistOrExeptionAsync(id);
            var task = (await _projectTaskRepository.GetAsync(IRepository<ProjectTask>.FilterById(id))).FirstOrDefault();
            task.Performer = (await _userRepository.GetAsync(IRepository<User>.FilterById(task.PerformerId))).FirstOrDefault();
            return _mapper.Map<ProjectTaskDTO>(task);
        }
        public async Task<ProjectTaskDTO> PostAsync(ProjectTaskCreateDTO task)
        {
            await _existService.UserIsExistOrExeptionAsync(task.PerformerId.Value);
            await _existService.ProjectIsExistOrExeptionAsync(task.ProjectId.Value);
            var newTask = _mapper.Map<ProjectTask>(task);
            await _projectTaskRepository.CreateAsync(newTask);
            return await GetAsync(newTask.Id);
        }
        public async Task<ProjectTaskDTO> UpdateAsync(ProjectTaskUpdateDTO updateTask)
        {
            await _existService.UserIsExistOrExeptionAsync(updateTask.PerformerId.Value);
            await _existService.ProjectTaskIsExistOrExeptionAsync(updateTask.Id.Value);
            var task = (await _projectTaskRepository.GetAsync(IRepository<ProjectTask>.FilterById(updateTask.Id.Value))).FirstOrDefault();
            task.Name = updateTask.Name;
            task.Description = updateTask.Description;
            task.FinishedAt = updateTask.FinishedAt.Value;
            task.State = updateTask.State;
            task.ProjectId = updateTask.ProjectId.Value;
            task.PerformerId = updateTask.PerformerId.Value;
            await _projectTaskRepository.UpdateAsync(task);
            return await GetAsync(updateTask.Id.Value);
        }
        public async Task DeleteAsync(int id)
        {
            await _existService.ProjectTaskIsExistOrExeptionAsync(id);
            await _projectTaskRepository.DeleteAsync(id);
        }
        public async Task<IEnumerable<ProjectTaskDTO>> GetTasksByUserAsync(int userId)
        {
            await _existService.UserIsExistOrExeptionAsync(userId);
            return (await GetAllAsync()).Where(t => t.Performer?.Id == userId && t.Name.Length < 45);
        }
        public async Task<IEnumerable<FinishedTaskDTO>> GetFinishedTasksAsync(int userId)
        {
            await _existService.UserIsExistOrExeptionAsync(userId);
            return _mapper.Map<IEnumerable<FinishedTaskDTO>>((await GetAllAsync()).Where(t => t.Performer?.Id == userId && t.State == TaskState.Finished && t.FinishedAt.Year == 2020));
        }
        public async Task<IEnumerable<ProjectTaskDTO>> GetNotFinishedTasksAsync(int userId)
        {
            await _existService.UserIsExistOrExeptionAsync(userId);
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>((await GetAllAsync()).Where(t => t.Performer?.Id == userId && t.State == TaskState.Created || t.State == TaskState.Started));
        }
        public async Task<IEnumerable<ProjectTaskDTO>> GetNotFinishedTasksAsync()
        {
            return _mapper.Map<IEnumerable<ProjectTaskDTO>>((await GetAllAsync()).Where(t => t.State == TaskState.Created || t.State == TaskState.Started));
        }
    }
}
