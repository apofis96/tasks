﻿using AutoMapper;
using EF.Common.DTO.Project;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using System.Collections.Generic;
using System.Linq;
using EF.Common;
using System.Threading.Tasks;

namespace EF.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IExistService _existService;
        private readonly IMapper _mapper;
        public ProjectService(IRepository<Project> projectRepository, IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository, IRepository<Team> teamRepository, IMapper mapper, IExistService existService)
        {
            _projectRepository = projectRepository;
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
            _teamRepository = teamRepository;
            _existService = existService;
            _mapper = mapper;
        }
        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var taskProjects = _projectRepository.GetAsync();
            var taskTasks = _projectTaskRepository.GetAsync();
            var taskUsers = _userRepository.GetAsync();
            var taskTeams = _teamRepository.GetAsync();
            await Task.WhenAll(taskProjects, taskTasks, taskUsers, taskTeams);
            var projects = (await taskProjects).GroupJoin((await taskTasks).GroupJoin(await taskUsers,
                t => t.PerformerId, u => u.Id, (t, u) =>
                {
                    t.Performer = u.FirstOrDefault();
                    return t;
                }),
                p => p.Id, ts => ts.ProjectId, (p, ts) =>
                {
                    p.Tasks = ts.ToList();
                    return p;
                }).GroupJoin(await taskUsers,
                p => p.AuthorId, u => u.Id, (p, u) =>
                {
                    p.Author = u.FirstOrDefault();
                    return p;
                }).GroupJoin(await taskTeams,
                p => p.TeamId, t => t.Id, (p, t) =>
                {
                    p.Team = t.FirstOrDefault();
                    return p;
                });
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
        public async Task<ProjectDTO> GetAsync(int id)
        {
            await _existService.ProjectIsExistOrExeptionAsync(id);
            var project = (await _projectRepository.GetAsync(IRepository<Project>.FilterById(id))).FirstOrDefault();
            var taskTasks = _projectTaskRepository.GetAsync();
            var taskUsers = _userRepository.GetAsync(IRepository<User>.FilterById(project.AuthorId.GetValueOrDefault()));
            var taskTeams = _teamRepository.GetAsync(IRepository<Team>.FilterById(project.TeamId.GetValueOrDefault()));
            await Task.WhenAll(taskTasks, taskUsers, taskTeams);
            project.Author = (await taskUsers).FirstOrDefault();
            project.Tasks = (await taskTasks).Where(t => t.ProjectId == project.Id).GroupJoin(await taskUsers,
            t => t.PerformerId, u => u.Id, (t, u) =>
            {
                t.Performer = u.FirstOrDefault();
                return t;
            });
            project.Team = (await taskTeams).FirstOrDefault();
            return _mapper.Map<ProjectDTO>(project);
        }
        public async Task<ProjectDTO> PostAsync(ProjectCreateDTO project)
        {
            await _existService.UserIsExistOrExeptionAsync(project.AuthorId.Value);
            await _existService.TeamIsExistOrExeptionAsync(project.TeamId.Value);
            var newProject = _mapper.Map<Project>(project);
            await _projectRepository.CreateAsync(newProject);
            return await GetAsync(newProject.Id);
        }
        public async Task<ProjectDTO> UpdateAsync(ProjectUpdateDTO updateProject)
        {
            await _existService.UserIsExistOrExeptionAsync(updateProject.AuthorId.Value);
            await _existService.TeamIsExistOrExeptionAsync(updateProject.TeamId.Value);
            await _existService.ProjectIsExistOrExeptionAsync(updateProject.Id.Value);
            var project = (await _projectRepository.GetAsync(IRepository<Project>.FilterById(updateProject.Id.Value))).FirstOrDefault();
            project.Name = updateProject.Name;
            project.Description = updateProject.Description;
            project.Deadline = updateProject.Deadline.Value;
            project.AuthorId = updateProject.AuthorId;
            project.TeamId = updateProject.TeamId;
            await _projectRepository.UpdateAsync(project);
            return await GetAsync(updateProject.Id.Value);
        }
        public async Task DeleteAsync(int id)
        {
            await _existService.ProjectIsExistOrExeptionAsync(id);
            await _projectRepository.DeleteAsync(id);
        }
        public async Task<IEnumerable<TaskCountDTO>> GetTasksCountByUserAsync(int userId)
        {
            await _existService.UserIsExistOrExeptionAsync(userId);
            return _mapper.Map<IEnumerable<TaskCountDTO>>((await GetAllAsync()).Where(p => p.Author?.Id == userId).ToDictionary(k => k, k => k.Tasks.Where(t => t.Performer?.Id == userId).Count()));
        }
        public async Task<UserStatisticDTO> GetUserStatisticsAsync(int userId)
        {
            var taskProjects = await GetAllAsync();
            var taskTasks = await _projectTaskRepository.GetAsync();
            await _existService.UserIsExistOrExeptionAsync(userId);
            var statistic = taskProjects.Where(p =>p.Author?.Id == userId).OrderByDescending(p => p.CreatedAt).GroupJoin(taskTasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt),
                p => p.Author.Id, t => t.PerformerId, (p, t) => new UserStatisticDTO
                {
                    User = p.Author,
                    LastProject = p,
                    LastProjectTasksCount = p.Tasks.Count(),
                    NotFinishedTasksCount = t.Where(t => t.State != TaskState.Finished).Count(),
                    LongestTask = _mapper.Map<ProjectTaskDTO>(t.FirstOrDefault())
                }).FirstOrDefault();
            statistic.LongestTask.Performer = statistic.User;
            return statistic;
        }
        public async Task<IEnumerable<ProjectStatisticDTO>> GetProjectStatisticsAsync()
        {
            var taskUsers = await _userRepository.GetAsync();
            return (await GetAllAsync()).Select(p => new ProjectStatisticDTO
            {
                Project = p,
                LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                MembersTeamCount = (taskUsers).Where(u => u.TeamId == p.Team?.Id && (p.Description.Length > 20 || p.Tasks.Count() < 3)).Count()
            });
        }
    }
}
