﻿using EF.Common.DTO.Project;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectDTO>> GetAllAsync();
        Task<ProjectDTO> GetAsync(int id);
        Task<ProjectDTO> PostAsync(ProjectCreateDTO project);
        Task<ProjectDTO> UpdateAsync(ProjectUpdateDTO project);
        Task DeleteAsync(int id);
        Task<IEnumerable<TaskCountDTO>> GetTasksCountByUserAsync(int userId);
        Task<UserStatisticDTO> GetUserStatisticsAsync(int userId);
        Task<IEnumerable<ProjectStatisticDTO>> GetProjectStatisticsAsync();
    }
}
