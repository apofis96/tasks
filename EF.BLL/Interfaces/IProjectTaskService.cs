﻿using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.BLL.Interfaces
{
    public interface IProjectTaskService
    {
        Task<IEnumerable<ProjectTaskDTO>> GetAllAsync();
        Task<ProjectTaskDTO> GetAsync(int id);
        Task<ProjectTaskDTO> PostAsync(ProjectTaskCreateDTO projectTask);
        Task<ProjectTaskDTO> UpdateAsync(ProjectTaskUpdateDTO projectTask);
        Task DeleteAsync(int id);
        Task<IEnumerable<ProjectTaskDTO>> GetTasksByUserAsync(int userId);
        Task<IEnumerable<FinishedTaskDTO>> GetFinishedTasksAsync(int userId);
        Task<IEnumerable<ProjectTaskDTO>> GetNotFinishedTasksAsync(int userId);
        Task<IEnumerable<ProjectTaskDTO>> GetNotFinishedTasksAsync();
    }
}
