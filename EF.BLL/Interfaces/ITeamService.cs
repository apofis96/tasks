﻿using EF.Common.DTO.Team;
using EF.Common.DTO.QueryResults;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EF.BLL.Interfaces
{
    public interface ITeamService
    {
        Task<IEnumerable<TeamDTO>> GetAllAsync();
        Task<TeamDTO> GetAsync(int id);
        Task<TeamDTO> PostAsync(TeamCreateDTO team);
        Task<TeamDTO> UpdateAsync(TeamUpdateDTO team);
        Task DeleteAsync(int id);
        Task<IEnumerable<TeamMembersDTO>> GetTeamMembersAsync();
    }
}
