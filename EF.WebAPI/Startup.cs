using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using EF.WebAPI.Extensions;
using Project_Structure.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Project_Structure.WebAPI.Extensions;
//using EF.WebAPI.IntegrationTests;

namespace EF.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbContext, EFContext>(options => options.UseSqlServer(Configuration.GetConnectionString("LocalDBConnection")), ServiceLifetime.Transient);
            services.RegisterAutoMapper();
            services.RegisterCustomServices();
            services.AddControllers();
            services.AddMvcCore(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
