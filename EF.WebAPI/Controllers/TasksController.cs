﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.ProjectTask;
using EF.Common.DTO.QueryResults;
using EF.BLL.Interfaces;
using System.Threading.Tasks;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly IProjectTaskService _taskService;
        public TasksController(IProjectTaskService taskService)
        {
            _taskService = taskService;
        }
        [HttpGet]
        public async Task<IEnumerable<ProjectTaskDTO>> GetAsync()
        {
            return await _taskService.GetAllAsync();
        }
        [HttpGet("{id}", Name = "TaskGet")]
        public async Task<ActionResult<ProjectTaskDTO>> GetAsync(int id)
        {
            return Ok(await _taskService.GetAsync(id));
        }
        [HttpPost]
        public async Task<ActionResult<ProjectTaskDTO>> PostAsync([FromBody] ProjectTaskCreateDTO task)
        {
            
            var newTask = await _taskService.PostAsync(task);
            return CreatedAtRoute("TaskGet", new { newTask.Id }, newTask);
        }
        [HttpPut]
        public async Task<ActionResult<ProjectTaskDTO>> PutAsync([FromBody] ProjectTaskUpdateDTO task)
        {
            
            var updateTask = await _taskService.UpdateAsync(task);
            return CreatedAtRoute("TaskGet", new { updateTask.Id }, updateTask);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _taskService.DeleteAsync(id);
            return NoContent();
        }
        [HttpGet("TasksByUser/{id}")]
        public async Task<ActionResult<IEnumerable<ProjectTaskDTO>>> GetTasksCountByUserAsync(int id)
        {
            return Ok(await _taskService.GetTasksByUserAsync(id));
        }
        [HttpGet("FinishedTasks/{id}")]
        public async Task<ActionResult<IEnumerable<FinishedTaskDTO>>> GetFinishedTasksAsync(int id)
        {
            return Ok(await _taskService.GetFinishedTasksAsync(id));
        }
        [HttpGet("NotFinishedTasks/{id}")]
        public async Task<ActionResult<IEnumerable<ProjectTaskDTO>>> GetNotFinishedTasksAsync(int id)
        {
            return Ok(await _taskService.GetNotFinishedTasksAsync(id));
        }
        [HttpGet("NotFinishedTasks")]
        public async Task<ActionResult<IEnumerable<ProjectTaskDTO>>> GetNotFinishedTasksAsync()
        {
            return Ok(await _taskService.GetNotFinishedTasksAsync());
        }
    }
}
