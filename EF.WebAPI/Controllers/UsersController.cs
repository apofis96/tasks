﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EF.Common.DTO.QueryResults;
using EF.Common.DTO.User;
using EF.BLL.Interfaces;
using System.Threading.Tasks;

namespace EF.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public async Task<IEnumerable<UserDTO>> GetAsync()
        {
            return await _userService.GetAllAsync();
        }
        [HttpGet("{id}", Name = "UserGet")]
        public async Task<ActionResult<UserDTO>> GetAsync(int id)
        {
            return Ok(await _userService.GetAsync(id));
        }
        [HttpPost]
        public async Task<ActionResult<UserDTO>> PostAsync([FromBody] UserCreateDTO user)
        {            
            var newUser = await _userService.PostAsync(user);
            return CreatedAtRoute("UserGet", new { newUser.Id }, newUser);

        }
        [HttpPut]
        public async Task<ActionResult<UserDTO>> PutAsync([FromBody] UserUpdateDTO user)
        {
            var updateUser = await _userService.UpdateAsync(user);
            return CreatedAtRoute("UserGet", new { updateUser.Id }, updateUser);

        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            await _userService.DeleteAsync(id);
            return NoContent();

        }
        [HttpGet("UserTasks")]
        public async Task<ActionResult<IEnumerable<UserTasksDTO>>> GetUserTasksAsync()
        {
            return Ok(await _userService.GetUserTasksAsync());
        }
    }
}
