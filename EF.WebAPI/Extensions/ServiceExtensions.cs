﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using EF.DAL.Repositories;
using System.Reflection;

namespace EF.WebAPI.Extensions
{
    public static class ServiceExtensions
    {
        public static void RegisterCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IRepository<User>, Repository<User>>();
            services.AddScoped<IRepository<Team>, Repository<Team>>();
            services.AddScoped<IRepository<ProjectTask>, Repository<ProjectTask>>();
            services.AddScoped<IRepository<Project>, Repository<Project>>();
            services.AddScoped<IExistService, ExistService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IProjectTaskService, ProjectTaskService>();
            services.AddScoped<IProjectService, ProjectService>();
        }
        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<ProjectTaskProfile>();
                cfg.AddProfile<ProjectProfile>();
            },
            Assembly.GetExecutingAssembly());
        }
    }
}
