﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Net;
namespace Project_Structure.WebAPI.Extensions
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            string errorCode;
            context.HttpContext.Response.ContentType = "application/json";
            switch (context.Exception)
            {
                case InvalidOperationException _:
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    errorCode = HttpStatusCode.NotFound.ToString();
                    break;
                case ArgumentException _:
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    errorCode = HttpStatusCode.BadRequest.ToString();
                    break;
                default:
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    errorCode = HttpStatusCode.InternalServerError.ToString();
                    break;
            }

            context.Result = new JsonResult(new
            {
                error = context.Exception.Message,
                code = errorCode
            });
        }
    }
}