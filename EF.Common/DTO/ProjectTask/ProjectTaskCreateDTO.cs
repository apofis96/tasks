﻿using System.ComponentModel.DataAnnotations;
using EF.Common;
using System;

namespace EF.Common.DTO.ProjectTask
{
    public class ProjectTaskCreateDTO
    {
        [Required]
        [StringLength(100, MinimumLength = 2)]
        public string Name { get; set; }
        [Required]
        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }
        [Required]
        public DateTime? FinishedAt { get; set; }
        [Required]
        [EnumDataType(typeof(TaskState))]
        public TaskState State { get; set; }
        [Required]
        public int? ProjectId { get; set; }
        [Required]
        public int? PerformerId { get; set; }
    }
}
