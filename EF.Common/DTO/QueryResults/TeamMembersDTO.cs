﻿using EF.Common.DTO.User;
using System.Collections.Generic;

namespace EF.Common.DTO.QueryResults
{
    public class TeamMembersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Members { get; set; }

    }
}
