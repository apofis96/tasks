﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using EF.UI.Menus;

namespace EF.UI
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static string host = "https://localhost:5001/";
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        static async Task Main(string[] args)
        {
            client.BaseAddress = new Uri(host);
            await MainMenu.Menu(client, options);
            //await Run();
        }
        static async Task Run()
        {
            await MainMenu.Menu(client, options);
        }
            
    }
}

