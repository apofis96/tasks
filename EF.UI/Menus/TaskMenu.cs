﻿using EF.Common;
using EF.Common.DTO.ProjectTask;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Timers;

namespace EF.UI.Menus
{
    public class TaskMenu
    {
        HttpClient _client;
        JsonSerializerOptions _options;
        public TaskMenu(HttpClient client, JsonSerializerOptions options)
        {
            _client = client;
            _options = options;
        }
        static string subpath = "Tasks";
        public async Task Menu()
        {
            string warning = "Sequence output is limited to three objects!";
            string pause = "Press any key to continue";
            string subMenuItems = @"1 : ReadAll
2 : Read(id)
3 : Create
4 : Update
5 : Delete
0 : Exit.";
            
            Console.WriteLine(subMenuItems);
            bool loop = true;
            while (loop)
            {
                Console.Write("Task menu input: ");
                char menu = Console.ReadKey().KeyChar;
                Console.WriteLine("\n================================");
                switch (menu)
                {
                    case '1':
                        Console.WriteLine(warning);
                        Console.WriteLine(pause);
                        Console.ReadKey();
                        Console.WriteLine(JsonSerializer.Serialize((await JsonSerializer.DeserializeAsync<IEnumerable<ProjectTaskDTO>>(await _client.GetStreamAsync(subpath), _options)).Take(3), _options));
                        break;
                    case '2':
                        Console.WriteLine(JsonSerializer.Serialize(await JsonSerializer.DeserializeAsync<ProjectTaskDTO>(await _client.GetStreamAsync(subpath + "/" + MenuInputs.IdInput()), _options), _options));
                        break;
                    case '3':
                        var newTask = new ProjectTaskCreateDTO();
                        Console.Write("Name: ");
                        newTask.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        newTask.Description = Console.ReadLine();
                        Console.Write("FinishedAt: ");
                        newTask.FinishedAt = MenuInputs.DateTimeInput();
                        Console.Write("State ");
                        newTask.State = (TaskState)MenuInputs.IdInput();
                        Console.Write("ProjectId ");
                        newTask.ProjectId = MenuInputs.IdInput();
                        Console.Write("PerformerId ");
                        newTask.PerformerId = MenuInputs.IdInput();
                        HttpResponseMessage response = await _client.PostAsync(subpath, new StringContent(JsonSerializer.Serialize(newTask), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(await JsonSerializer.DeserializeAsync<ProjectTaskDTO>(await response.Content.ReadAsStreamAsync(), _options), _options));
                        Console.WriteLine("Done");
                        break;
                    case '4':
                        var updateTask = new ProjectTaskUpdateDTO();
                        updateTask.Id = MenuInputs.IdInput();
                        Console.Write("Name: ");
                        updateTask.Name = Console.ReadLine();
                        Console.Write("Description: ");
                        updateTask.Description = Console.ReadLine();
                        Console.Write("FinishedAt: ");
                        updateTask.FinishedAt = MenuInputs.DateTimeInput();
                        Console.Write("State ");
                        updateTask.State = (TaskState)MenuInputs.IdInput();
                        Console.Write("ProjectId ");
                        updateTask.ProjectId = MenuInputs.IdInput();
                        Console.Write("PerformerId ");
                        updateTask.PerformerId = MenuInputs.IdInput();
                        response = await _client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateTask), Encoding.UTF8, "application/json"));
                        if (response.StatusCode != HttpStatusCode.Created)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine(JsonSerializer.Serialize(await JsonSerializer.DeserializeAsync<ProjectTaskDTO>(await response.Content.ReadAsStreamAsync(), _options), _options));
                        Console.WriteLine("Done");
                        break;
                    case '5':
                        response = await _client.DeleteAsync(subpath + "/" + MenuInputs.IdInput());
                        if (response.StatusCode != HttpStatusCode.NoContent)
                        {
                            Console.WriteLine(response.StatusCode);
                            return;
                        }
                        Console.WriteLine("Done");
                        break;
                    case '0':
                        loop = false;
                        break;
                    default:
                        Console.WriteLine(subMenuItems);
                        break;
                }
            }
        }
        public async Task<int> MarkRandomTaskWithDelay(double delay)
        {
            Timer timer = new Timer(delay);
            timer.AutoReset = false;
            var tcs = new TaskCompletionSource<int>();
            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    var tasks = await JsonSerializer.DeserializeAsync<IEnumerable<ProjectTaskDTO>>(await _client.GetStreamAsync(subpath + "/NotFinishedTasks"), _options);
                    if (tasks.Count() == 0)
                    {
                        tcs.TrySetCanceled();
                    }
                    else
                    {
                        Random rnd = new Random();
                        var task = tasks.ElementAt(rnd.Next(0, tasks.Count() - 1));
                        var updateTask = new ProjectTaskUpdateDTO()
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Description = task.Description,
                            FinishedAt = DateTime.Now,
                            State = TaskState.Finished,
                            ProjectId = task.ProjectId,
                            PerformerId = task.Performer.Id
                        };
                        var response = await _client.PutAsync(subpath, new StringContent(JsonSerializer.Serialize(updateTask), Encoding.UTF8, "application/json"));
                        response.EnsureSuccessStatusCode();
                        tcs.TrySetResult(task.Id);
                    }
                }
                catch (Exception exc)
                {
                    tcs.TrySetException(exc);
                }
            };
            timer.Enabled = true;
            return await tcs.Task;
        }
    }
}