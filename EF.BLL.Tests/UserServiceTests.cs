using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.Common.DTO.User;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EF.BLL.Tests
{
    public class UserServiceTests
    {
        readonly UserService _userService;
        readonly FakeRepository<User> _userRepository;
        readonly IRepository<ProjectTask> _taskRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public UserServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _taskRepository = A.Fake<IRepository<ProjectTask>>();
            _userRepository = A.Fake<FakeRepository<User>>(options => options.CallsBaseMethods());
            _existExistService = A.Fake<IExistService>();
            _userService = new UserService(_userRepository, _taskRepository, _mapper, _existExistService);
            Seed();
        }
        private void Seed()
        {
            _userRepository._entities.Add(new User()
            {
                Id = 1,
                TeamId = 100,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            });
            _userRepository._entities.Add(new User()
            {
                Id = 2,
                TeamId = 100,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now
            });
        }
        [Fact]
        public async Task AddNewUserWithNotExistTeamId_ThenThrowInvalidOperation()
        {
            var newUser = new UserCreateDTO()
            {
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                TeamId = 1
            };
            A.CallTo(() => _existExistService.TeamIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("Team not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _userService.PostAsync(newUser));
        }
        [Fact]
        public async Task AddNewUserWithoutTeamId_ThenUserRepositoryCreateIsHappened()
        {
            var newUser = new UserCreateDTO()
            {
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
            };
            A.CallTo(() => _existExistService.TeamIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("Team not found"));

            await _userService.PostAsync(newUser);

            A.CallTo(() => _userRepository.CreateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }
        [Fact]
        public async Task ChangeUserTeam_WhenUserNotExist_ThenThrowInvalidOperation()
        {
            var updateUser = new UserUpdateDTO()
            {
                Id = 10,
                TeamId = 101,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
            };
            A.CallTo(() => _existExistService.UserIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("User not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _userService.UpdateAsync(updateUser));
        }
        [Fact]
        public async Task ChangeUserTeam_WhenUserAndTeamExist_ThenReturnedUserTeamChanged()
        {
            var updateUser = new UserUpdateDTO()
            {
                Id = 1,
                TeamId = 101,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Now,
            };

            var returnedUser = await _userService.UpdateAsync(updateUser);

            A.CallTo(() => _userRepository.UpdateAsync(A<User>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(1, returnedUser.Id);
            Assert.Equal(101, returnedUser.TeamId);
        }
        [Fact]
        public async Task DeleteUser_WhenUserNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("User not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _userService.DeleteAsync(10));
        }
        [Fact]
        public async Task DeleteUser_WhenUserExister_ThenUserRepositoryDeleteIsHappened()
        {
            await _userService.DeleteAsync(1);

            A.CallTo(() => _userRepository.DeleteAsync(A<int>._)).MustHaveHappenedOnceExactly();
            Assert.Single(_userRepository._entities);
        }
    }
}
