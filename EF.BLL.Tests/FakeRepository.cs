﻿using EF.DAL.Interfaces;
using EF.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EF.BLL.Tests
{
    public class FakeRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        public readonly List<TEntity> _entities = new List<TEntity>();
        public FakeRepository() {}
        public virtual async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            List<TEntity> entities = new List<TEntity>();
            if (filter != null)
            {
                entities = _entities.AsQueryable().Where(filter).ToList();
            }
            else
            {
                entities = _entities.ToList();
            }
            return entities;
        }
        public virtual async Task CreateAsync(TEntity entity)
        {
            await Task.Run(() => _entities.Add(entity));
        }
        public virtual async Task UpdateAsync(TEntity entity)
        {
            var update = _entities.FirstOrDefault(e => e.Id == entity.Id);
            if (update != null)
            {
                await DeleteAsync(update);
                await CreateAsync(entity);
            }
        }
        public virtual async Task DeleteAsync(int id)
        {
            var delete = _entities.FirstOrDefault(e => e.Id == id);
            if (delete != null)
            {
                await DeleteAsync(delete);
            }
        }
        public virtual async Task DeleteAsync(TEntity entity)
        {
            _entities.Remove(entity);
        }
    }
}
