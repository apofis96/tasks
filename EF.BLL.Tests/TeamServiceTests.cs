using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EF.BLL.Tests
{
    public class TeamServiceTests
    {
        readonly TeamService _teamService;
        readonly FakeRepository<Team> _teamRepository;
        readonly IRepository<User> _userRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public TeamServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProjectTaskProfile());
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new TeamProfile());
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _userRepository = A.Fake<IRepository<User>>();
            _teamRepository = A.Fake<FakeRepository<Team>>(options => options.CallsBaseMethods());
            _existExistService = A.Fake<IExistService>();
            _teamService = new TeamService(_teamRepository, _userRepository, _mapper, _existExistService);
            Seed();
        }
        private void Seed()
        {
            _teamRepository._entities.Add(new Team()
            {
                Id = 1,
                Name = "testTeam1",
                Description = "testDescription2",
                CreatedAt = DateTime.Now
            });
            _teamRepository._entities.Add(new Team()
            {
                Id = 2,
                Name = "testTeam2",
                Description = "testDescription2",
                CreatedAt = DateTime.Now
            });
            _teamRepository._entities.Add(new Team()
            {
                Id = 3,
                Name = "testTeam3",
                Description = "testDescription3",
                CreatedAt = DateTime.Now
            });
            var users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                TeamId = 1,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2008-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2018-10-29T00:26:53.01"),
            });
            users.Add(new User()
            {
                Id = 2,
                TeamId = 1,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2007-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2019-10-29T00:26:53.01"),
            });
            users.Add(new User()
            {
                Id = 3,
                TeamId = 2,
                Email = "test@example.com",
                FirstName = "first",
                LastName = "last",
                Birthday = DateTime.Parse("2017-10-29T00:26:53.01"),
                RegisteredAt = DateTime.Parse("2009-10-29T00:26:53.01"),
            });
            A.CallTo(() => _userRepository.GetAsync(null)).Returns(users);
        }
        [Fact]
        public async Task GetTeamMembers_WhenOneTeamWithMembers_ThenReturnedListLength1()
        {
            var result = await _teamService.GetTeamMembersAsync();

            Assert.Single(result);
            Assert.Equal(2, result.First().Members.First().Id);
        }
        [Fact]
        public async Task DeleteTeam_WhenTeamNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.TeamIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("Team not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _teamService.DeleteAsync(10));
        }
        [Fact]
        public async Task DeleteTeam_WhenTeamExister_ThenTeamRepositoryDeleteIsHappened()
        {
            await _teamService.DeleteAsync(1);

            A.CallTo(() => _teamRepository.DeleteAsync(A<int>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(2, _teamRepository._entities.Count);
        }
    }
}
