using AutoMapper;
using EF.BLL.Interfaces;
using EF.BLL.MappingProfiles;
using EF.BLL.Services;
using EF.Common;
using EF.Common.DTO.ProjectTask;
using EF.DAL.Interfaces;
using EF.DAL.Models;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EF.BLL.Tests
{
    public class ProjectTaskServiceTests
    {
        readonly ProjectTaskService _taskService;
        readonly IRepository<User> _userRepository;
        readonly FakeRepository<ProjectTask> _taskRepository;
        readonly IExistService _existExistService;
        readonly IMapper _mapper;
        public ProjectTaskServiceTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ProjectTaskProfile());
                cfg.AddProfile(new ProjectProfile());
                cfg.AddProfile(new TeamProfile());
                cfg.AddProfile(new UserProfile());
            });
            _mapper = config.CreateMapper();
            _userRepository = A.Fake<IRepository<User>>();
            _taskRepository = A.Fake<FakeRepository<ProjectTask>>(options => options.CallsBaseMethods());
            _existExistService = A.Fake<IExistService>();
            _taskService = new ProjectTaskService(_taskRepository, _userRepository, _mapper, _existExistService);
            Seed();
        }
        private void Seed()
        {
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 1,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 2,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description2",
                Name = "Name3",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Finished
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 3,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description3",
                Name = "LongName1234567891234567891345678913465891324657891324567498732132456764315643165432132456456412345678912345678913456789134658913246578913245674987321324567643156431654321324564564",
                PerformerId = 1,
                ProjectId = 1,
                State = TaskState.Started
            });
            _taskRepository._entities.Add(new ProjectTask()
            {
                Id = 4,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description4",
                Name = "Name4",
                PerformerId = 2,
                ProjectId = 1,
                State = TaskState.Finished
            });
            List<User> users = new List<User>();
            users.Add(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            users.Add(new User()
            {
                Id = 2,
                FirstName = "FirstName2",
                LastName = "LastName2",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 2
            });
            A.CallTo(() => _userRepository.GetAsync(null)).Returns(users);
        }
        [Fact]
        public async Task MarkTaskAsFinished_WhenTaskNotExist_ThenInvalidOperationException()
        {
           var updateTask = new ProjectTaskUpdateDTO()
            {
                Id = 1,
                Name = "testName1",
                Description = "testDescription1",
                FinishedAt = DateTime.Now,
                State = TaskState.Finished,
                ProjectId = 1,
                PerformerId = 1
            };

            A.CallTo(() => _existExistService.ProjectTaskIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("Task not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.UpdateAsync(updateTask));
        }
        [Fact]
        public async Task MarkTaskAsFinished_WhenTaskExist_ThenReturnedTaskFinished()
        {
            var updateTask = new ProjectTaskUpdateDTO()
            {
                Id = 1,
                Name = "testName1",
                Description = "testDescription1",
                FinishedAt = DateTime.Now,
                State = TaskState.Finished,
                ProjectId = 1,
                PerformerId = 1
            };

            var returnedTask = await _taskService.UpdateAsync(updateTask);

            A.CallTo(() => _taskRepository.UpdateAsync(A<ProjectTask>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(1, returnedTask.Id);
            Assert.Equal(TaskState.Finished, returnedTask.State);
        }
        [Fact]
        public async Task GetTasksByUser_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("User not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.GetTasksByUserAsync(10));
        }
        [Fact]
        public async Task GetTasksByUser_WhenUserExistAndTwoTasks_ThenReturnedListLength2()
        {
            var result = await _taskService.GetTasksByUserAsync(1);

            Assert.Equal(2, result.Count());
        }
        [Fact]
        public async Task GetFinishedTasks_WhenUserExistAndOneTask_ThenReturnedListLength1()
        {
            var result = await _taskService.GetFinishedTasksAsync(2);

            Assert.Single(result);
            Assert.Equal(4, result.First().Id);
        }
        [Fact]
        public async Task GetFinishedTasks_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("User not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.GetFinishedTasksAsync(1));
        }
        [Fact]
        public async Task GetNotFinishedTasks_WhenUseExistAndOneTask_ThenInvalidOperationException()
        {
            var result = await _taskService.GetNotFinishedTasksAsync(1);

            Assert.Single(result);
            Assert.Equal(3, result.First().Id);
        }
        [Fact]
        public async Task GetNotFinishedTasks_WhenUserNotExist_ThenInvalidOperationException()
        {
            A.CallTo(() => _existExistService.UserIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("User not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.GetNotFinishedTasksAsync(10));
        }
        [Fact]
        public async Task DeleteTask_WhenTaskNotExister_ThenThrowInvalidOperation()
        {
            A.CallTo(() => _existExistService.ProjectTaskIsExistOrExeptionAsync(A<int>._)).Throws(new InvalidOperationException("Task not found"));

            await Assert.ThrowsAsync<InvalidOperationException>(() => _taskService.DeleteAsync(10));
        }
        [Fact]
        public async Task DeleteTask_WhenTaskExister_ThenTaskRepositoryDeleteIsHappened()
        {
            await _taskService.DeleteAsync(1);

            A.CallTo(() => _taskRepository.DeleteAsync(A<int>._)).MustHaveHappenedOnceExactly();
            Assert.Equal(3, _taskRepository._entities.Count);
        }
    }
}
