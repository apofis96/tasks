﻿using EF.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Project_Structure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasData(DeserializeEntity<Team>("PreloadedData\\teams.json"));
            modelBuilder.Entity<User>().HasData(DeserializeEntity<User>("PreloadedData\\users.json"));
            modelBuilder.Entity<Project>().HasData(DeserializeEntity<Project>("PreloadedData\\projects.json"));
            modelBuilder.Entity<ProjectTask>().HasData(DeserializeEntity<ProjectTask>("PreloadedData\\tasks.json"));
        }
        public static ICollection<TEntity> DeserializeEntity<TEntity>(string fileName)
        {
            return JsonSerializer.Deserialize<List<TEntity>>(File.ReadAllText(fileName), new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });
        }
    }
}
