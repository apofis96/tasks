﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EF.DAL.Models
{
    public class User : Entity
    {
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string LastName { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public User() : base()
        {
            RegisteredAt = DateTime.Now;
        }
    }
}
