﻿using EF.DAL.Interfaces;
using EF.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EF.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        private readonly DbContext _context;
        public Repository(DbContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> entities = _context.Set<TEntity>();
            if (filter != null)
            {
                entities = entities.Where(filter);
            }
            return await entities.ToListAsync();
        }
        public async Task CreateAsync(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(TEntity entity)
        {
            _context.Set<TEntity>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteAsync(int id)
        {
            await DeleteAsync(_context.Set<TEntity>().Find(id));
        }
        public async Task DeleteAsync(TEntity entity)
        {
            var entities = _context.Set<TEntity>();
            if (_context.Entry(entity).State == EntityState.Detached)
                entities.Attach(entity);
            entities.Remove(entity);
            await _context.SaveChangesAsync();
        }
    }
}
