﻿using System;
using System.Collections.Generic;
using System.Text;
using EF.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace EF.WebAPI.IntegrationTests
{
    public class TestingContext : DbContext
    {
        public TestingContext(DbContextOptions<TestingContext> options) : base(options) { }
        public DbSet<User> Users { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasData(new Team() 
            { 
                Id = 1,
                Name = "testTeam",
                Description = "testDescription",
                CreatedAt = DateTime.Now
            });
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = 1,
                FirstName = "FirstName1",
                LastName = "LastName1",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = 5,
                FirstName = "FirstName5",
                LastName = "LastName5",
                Email = "Email1@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            modelBuilder.Entity<User>().HasData(new User()
            {
                Id = 10,
                FirstName = "FirstName10",
                LastName = "LastName10",
                Email = "Email10@example.com",
                Birthday = DateTime.Now,
                RegisteredAt = DateTime.Now,
                TeamId = 1
            });
            modelBuilder.Entity<ProjectTask>().HasData(new ProjectTask()
            {
                Id = 1,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description1",
                Name = "Name1",
                PerformerId = 1,
                ProjectId = 1,
                State = Common.TaskState.Canceled
            }) ;
            modelBuilder.Entity<ProjectTask>().HasData(new ProjectTask()
            {
                Id = 10,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description10",
                Name = "Name10",
                PerformerId = 1,
                ProjectId = 1,
                State = Common.TaskState.Canceled
            });
            modelBuilder.Entity<ProjectTask>().HasData(new ProjectTask()
            {
                Id = 11,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description11",
                Name = "Name11",
                PerformerId = 5,
                ProjectId = 1,
                State = Common.TaskState.Created
            });
            modelBuilder.Entity<ProjectTask>().HasData(new ProjectTask()
            {
                Id = 12,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description12",
                Name = "Name12",
                PerformerId = 5,
                ProjectId = 2,
                State = Common.TaskState.Started
            });
            modelBuilder.Entity<ProjectTask>().HasData(new ProjectTask()
            {
                Id = 13,
                FinishedAt = DateTime.Now,
                CreatedAt = DateTime.Now,
                Description = "Description12",
                Name = "Name12",
                PerformerId = 5,
                ProjectId = 1,
                State = Common.TaskState.Finished
            });
        }
    }
}
