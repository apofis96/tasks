using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.Text.Json;
using EF.Common.DTO.User;

namespace EF.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task DeleteUser_WhenUserNotExist_ThanResponseCode404()
        {
            var httpResponse = await _client.DeleteAsync("/Users/42");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async Task DeleteUser_WhenUserExist_ThanResponseCode204()
        {
            var httpResponse = await _client.DeleteAsync("/Users/10");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);
        }
        [Fact]
        public async Task AddUser_WhenTeamNotExist_ThanResponseWithCode404()
        {
            var userJson = "{\"firstName\":\"Jordane\",\"lastName\":\"Walker\",\"email\":\"Jordane.Walker@gmail.com\",\"birthday\": \"2005-10-29T00:26:53.0193606\",\"teamId\": 42}";
            
            var httpResponse = await _client.PostAsync("/Users", new StringContent(userJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
        [Fact]
        public async Task AddUser_WhenTeamExist_ThanResponseWithCode201AndCorrespondedBody()
        {
            var userJson = "{\"firstName\":\"Jordane\",\"lastName\":\"Walker\",\"email\":\"Jordane.Walker@gmail.com\",\"birthday\": \"2005-10-29T00:26:53.0193606\",\"teamId\": 1}";
            
            var httpResponse = await _client.PostAsync("/Users", new StringContent(userJson, Encoding.UTF8, "application/json"));
            var newUser= JsonSerializer.Deserialize<UserCreateDTO>(userJson, options);
            var createdUser = JsonSerializer.Deserialize<UserDTO>(await httpResponse.Content.ReadAsStringAsync(), options);
            await _client.DeleteAsync("/Users/" + createdUser.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(newUser.FirstName, createdUser.FirstName);
            Assert.Equal(newUser.LastName, createdUser.LastName);
            Assert.Equal(newUser.Email, createdUser.Email);
            Assert.Equal(newUser.Birthday.Value, createdUser.Birthday);
            Assert.Equal(newUser.TeamId, createdUser.TeamId);
            }
        }
    }
