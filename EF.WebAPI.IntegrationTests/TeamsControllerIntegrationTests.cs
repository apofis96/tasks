using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.Text.Json;
using EF.Common.DTO.Team;

namespace EF.WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
            WriteIndented = true,
        };
        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Theory]
        [InlineData("{\"name\":\"t\",\"description\":\"testWrongDescription\"}")]
        [InlineData("{\"name\":\"testWrongName\",\"description\":\"testWrong\"}")]
        [InlineData("{\"description\":\"testWrongDescription\"}")]
        [InlineData("{\"name\":\"t\"}")]
        [InlineData("WrongData")]
        [InlineData("")]
        public async Task AddTeam_WhenWrongRequestBody_ThanResponseCode400(string teamJson)
        {
            var httpResponse = await _client.PostAsync("/Teams", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
        [Theory]
        [InlineData("{\"name\":\"testGoodName\",\"description\":\"testGoodDescription\"}")]
        [InlineData("{\"name\":\"testGoodName\"}")]
        public async Task AddTeam_WhenRequestBodyValid_ThanResponseWithCode201AndCorrespondedBody(string teamJson)
        {
            var httpResponse = await _client.PostAsync("/Teams", new StringContent(teamJson, Encoding.UTF8, "application/json"));

            var newTeam = JsonSerializer.Deserialize<TeamCreateDTO>(teamJson, options);
            var createdTeam = JsonSerializer.Deserialize<TeamDTO>(await httpResponse.Content.ReadAsStringAsync(), options);

            await _client.DeleteAsync("/Teams/" + createdTeam.Id);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(newTeam.Name, createdTeam.Name);
            Assert.Equal(newTeam.Description, createdTeam.Description);
        }
    }
}
